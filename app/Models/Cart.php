<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;


    public function books(){
        return $this->belongsTo(Book::class,'book_id');
    }

    public function customers(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function endDates(){
        return $this->belongsTo(EndDate::class,'end_id');
    }
}
