<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EndDate extends Model
{
    use HasFactory;

    public function Carts(){
        return $this->hasMany(EndDate::class);
    }
    public function renteds(){
        return $this->hasMany(Rented::class);
    }
}
