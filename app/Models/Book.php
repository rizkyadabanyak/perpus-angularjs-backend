<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function carts (){
        return $this->hasMany(Cart::class);
    }

    public function categories(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function renteds(){
        return $this->hasMany(Rented::class);
    }

}
