<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReturnResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cekStatus ($status){
        if ($status == 0){
            return 'belum di kembalikan';
        }else{
            return 'Sudah Di kembalikan';

        }
    }
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'book_id' => $this->books->id,
            'book' => $this->books->name,
            'quantity' => $this->quantity,
            'start_date' => $this->quantity,
            'status' => $this->cekStatus($this->status),
            'end_date' => $this->end_date,
        ];
    }
}
