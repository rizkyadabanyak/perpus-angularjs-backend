<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'category' => $this->categories->name,
            'img' => $this->img,
            'author' => $this->author,
            'year' => $this->year,
            'publisher' => $this->publisher,
            'rating' => $this->rating,
            'stock' => $this->stock,
            'dsc' => $this->dsc,
            'time' => $this->created_at->diffForHumans(),

        ];
    }
}
