<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Http\Resources\CartResource;
use App\Models\Book;
use App\Models\Cart;
use App\Models\EndDate;
use App\Models\Rented;
use App\Models\UniqCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\CommonMark\Inline\Element\Code;

class CartController extends Controller
{
    public function countCart(){
        $data = Cart::whereCustomerId(Auth::user()->id)->count();
        return response()->json($data);
    }

    public function actionCart(Request $request){
        try {

            $date = EndDate::find($request->end_id);
            $data = new Cart();
            $data->customer_id = Auth::user()->id;
            $data->book_id = $request->book_id;
            $data->quantity = $request->quantity;
            $data->end_date = Carbon::now()->addDays($date->count);

            $data->save();

            return response()->json([
                'status' => 'success',
                'msg' => 'success add to cart'
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'danger',
                'msg' => 'cant add book in cart'
            ]);
        }
    }

    public function getCart(){
        $data = Cart::whereCustomerId(Auth::user()->id)->get();
        return CartResource::collection($data);
    }

    public function deleteCart($id){
        Cart::where([
            'customer_id' => Auth::user()->id,
            'book_id' => $id
        ])->delete();

        return response()->json([
            'status' => 'success',
            'msg' => 'success delete book'
        ]);
    }
    public function deleteAllCart(){
        $carts = Cart::whereCustomerId(Auth::user()->id)->get();

        foreach ($carts as $cart){

            Cart::where([
                'customer_id' => Auth::user()->id,
                'book_id' => $cart->book_id
            ])->delete();
        }

        return response()->json([
            'status' => 'success',
            'msg' => 'success delete all items in cart'
        ]);
    }

    public function rent(){
        $nowYear = Carbon::now()->format('Y');
        $nowMonth = Carbon::now()->format('m');
        $nowDay = Carbon::now()->format('d');
        $uniq = rand(999,1);

        $code = 'ITPI'.$uniq.$nowDay.$nowMonth.$nowYear;

        $uniqCode = new UniqCode();
        $uniqCode->code = $code;
        $uniqCode->customer_id = Auth::user()->id;
        $uniqCode->save();

        $carts = Cart::whereCustomerId(Auth::user()->id)->get();
        foreach ($carts as $cart){
            $data = new Rented();
            $data->code = $code;
            $data->book_id = $cart->book_id;
            $data->quantity = $cart->quantity;
            $data->start_date = Carbon::now();
            $data->status = '0';
            $data->end_date = $cart->end_date;

            $data->save();

            $book = Book::find($cart->book_id);
            $book->stock = $book->stock - $cart->quantity;
            $book->save();

            Cart::where([
                'customer_id' => Auth::user()->id,
                'book_id' => $cart->book_id
            ])->delete();
        }

        return response()->json([
            'status' => 'success',
            'msg' => 'yey success',
            'code' => $code
        ]);

    }
}
