<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\Http\Resources\ReturnResource;
use App\Models\Customer;
use App\Models\Rented;
use App\Models\UniqCode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function cekAdmin(){
        $data = User::find(Auth::user()->id);

        return response()->json($data);
    }

    public function cek(){
        $data = Customer::find(Auth::user()->id);

        return response()->json($data);
    }
    public function histories(){
        $data = UniqCode::whereCustomerId(Auth::user()->id)->get();
//        dd($data);
        return response()->json($data);
    }
    public function return(Request $request){

        $data = Rented::whereCode($request->code)->get();
//        dd($data);
        return ReturnResource::collection($data);

    }

    public function returnAdmin(Request $request){

        $data = Rented::whereCode($request->code)->get();
//        dd($data);
        return ReturnResource::collection($data);

    }
    public function actionRetrun(Request $request){

        $data = Rented::where([
            'code' => $request->code,
            'book_id' => $request->book_id
        ])->first();


        $data->status = "1";
        $data->save();

        dd($data);
        return response()->json([
            'msg' => "berhasil di kembalikan"
        ]);

    }
    public function getPengambilan(Request $request){
        $data = UniqCode::whereCode($request->code)->first();

        return response()->json($data);
    }
    public function actionPengambilan(Request $request){
        $data = UniqCode::whereCode($request->code)->first();

        $data->status = 'taken';
        $data->save();

        return response()->json([
            'msg' => "berhasil di kembalikan"
        ]);
    }
}
