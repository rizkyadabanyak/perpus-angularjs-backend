<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\HistoriesResource;
use App\Models\UniqCode;
use Illuminate\Http\Request;

class historyAdminController extends Controller
{
    public function index(){
        $data = UniqCode::all();

        return HistoriesResource::collection($data);
    }
}
