<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Category::all();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validates = [
          'name' => 'required'

        ];
        $request->validate($validates);

        $data = new Category();
        $data->name = $request->name;
        $data->save();

        return response()->json([
           'msg' => 'success add category',
            'status' => 'success',
            'data'=> $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validates = [
            'name' => 'required'

        ];
        $request->validate($validates);

        $data = Category::find($id);
        $data->name = $request->name;
        $data->save();

        return response()->json([
            'msg' => 'success edit category',
            'status' => 'success',
            'data'=> $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::find($id);
        $data->delete();

        return response()->json([
            'msg' => 'success delete category',
            'status' => 'success',
        ]);
    }
}
