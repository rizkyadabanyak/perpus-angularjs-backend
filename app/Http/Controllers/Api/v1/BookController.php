<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::all();

        return BookResource::collection($data);

//        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validates = [
            'category_id' => 'required',
            'name' => 'required',
            'year' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'dsc' => 'required',
        ];
        $request->validate($validates);

        $data = new Book();

        $data->name = $request->name;
        $data->category_id = $request->category_id;
        $data->slug = \Str::slug($request->name);
        if ($request->file('image')){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $request->newImage;
        }

        $data->year = $request->year;
        $data->author = $request->author;
        $data->publisher = $request->publisher;
        $data->rating = 5;
        $data->stock = $request->stock;
        $data->dsc = $request->dsc;

        $data->save();

        return response()->json([
            'msg' => 'success add book',
            'status' => 'success',
            'data'=> $data
        ]);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return new BookResource($book);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $validates = [
            'category_id' => 'required',
            'name' => 'required',
            'year' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'rating' => 'required',
            'dsc' => 'required',
        ];

        $request->validate($validates);

        $data = Book::find($id);
        $data->name = $request->name;
        $data->category_id = $request->category_id;
        $data->slug = \Str::slug($request->name);
        if ($request->file('image')){
            $file = $request->file('image');
            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $imgDB = 'uploads/'.$newName;

            $request->image->move(public_path('uploads'), $newName);
            $data->img = $imgDB;
        }else{
            $data->img = $request->newImage;
        }

        $data->year = $request->year;
        $data->author = $request->author;
        $data->publisher = $request->publisher;
        $data->rating = $request->rating;
        $data->dsc = $request->dsc;
        $data->save();

        return response()->json([
            'msg' => 'success add book',
            'status' => 'success',
            'data'=> $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Book::find($id);

        $data->delete();

        return response()->json([
           'msg' => 'yey delete success',
           'status' => 'success'
        ]);

    }
}
