<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function regis(Request $request){
        $validates = [
            'name' => 'required',
            'email' => 'required|unique:customers',
            'password' => 'required'
        ];
        $request->validate($validates);

        $data = new Customer();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();

        if (!$token = auth('customer-api')->attempt($request->only('email','password'))){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal login'
            ]);
        }

        return response()->json([
            'token' => $token,
            'data' => $data,
            'status' => 'success',
            'msg' => 'regis success'
        ]);

    }
    public function login(Request $request){

        $validates = [
            'email' => 'required',
            'password' => 'required'
        ];

        $request->validate($validates);

        if (!$token = auth('customer-api')->attempt($request->only('email','password'))){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal login'
            ]);
        }

        return response()->json([
            'token' => $token,
            'status' => 'success',
            'msg' => 'yey success login'
        ]);

    }
    public function logout(Request $request)
    {
        auth('customer-api')->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'yey success logout'
        ]);
    }

    public function test (Request $request){
        try {
            return $request->user()->name;

        }catch (\Exception $e){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal mengambil profil'
            ]);
        }
    }
}
