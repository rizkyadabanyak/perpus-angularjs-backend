<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function regis(Request $request){
        $validates = [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ];
        $request->validate($validates);

        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();

        if (!$token = auth('api')->attempt($request->only('email','password'))){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal login'
            ]);
        }

        return response()->json([
            'token' => $token,
            'status' => 'success',
           'msg' => 'regis success'
        ]);

    }
    public function login(Request $request){

        $validates = [
            'email' => 'required',
            'password' => 'required'
        ];

        $request->validate($validates);

        if (!$token = auth('api')->attempt($request->only('email','password'))){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal login'
            ]);
        }

        return response()->json([
            'token' => $token,
            'status' => 'success',
            'msg' => 'yey success login'
        ]);

    }
    public function logout(Request $request)
    {
        auth('api')->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'yey success logout'
        ]);
    }

    public function test (Request $request){
        try {
            $data = Auth::user()->name;

            return $data;

        }catch (\Exception $e){
            return response()->json([
                'status' => 'danger',
                'msg' => 'gagal mengambil profil'
            ]);
        }
    }
}
