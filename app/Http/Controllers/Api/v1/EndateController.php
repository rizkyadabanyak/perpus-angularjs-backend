<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\EndDate;
use Illuminate\Http\Request;

class EndateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EndDate::all();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new EndDate();

        $data->name = $request->name;
        $data->count = $request->count;

        $data->save();

        return response()->json([
            'status' => 'success',
            'msg' => 'success add data'
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = EndDate::find($id);

        $data->name = $request->name;
        $data->count = $request->count;
        $data->save();

        return response()->json([
            'status' => 'success',
            'msg' => 'success edit data'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = EndDate::find($id);

        $data->delete();
        return response()->json([
            'status' => 'success',
            'msg' => 'success delete data'
        ]);
    }
}
