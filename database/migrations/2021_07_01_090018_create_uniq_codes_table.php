<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniqCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uniq_codes', function (Blueprint $table) {
            $table->integer('id')->nullable();
            $table->string('code')->unique();
            $table->foreignId('customer_id')->constrained('customers');
            $table->enum('status',['taken','not-take'])->default('not-take');
            $table->primary(['code']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uniq_codes');
    }
}
