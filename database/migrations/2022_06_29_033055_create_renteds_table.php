<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renteds', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->foreignId('book_id')->constrained('books');
            $table->integer('quantity');
            $table->dateTime('start_date');
            $table->enum('status',['0','1'])->default('0');
            $table->dateTime('end_date');
            $table->timestamps();

            $table->foreign('code')->references('code')->on('uniq_codes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renteds');
    }
}
