<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->middleware(['cors'])->group(function (){
    Route::get('books',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'index']);
    Route::get('categories',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'categories']);
    Route::get('books/{book}',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'detail']);
    Route::get('date',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'date']);

    Route::prefix('auth/admin')->group(function (){
        Route::post('regis',[\App\Http\Controllers\Api\v1\Auth\AuthController::class,'regis']);
        Route::post('login',[\App\Http\Controllers\Api\v1\Auth\AuthController::class,'login']);
        Route::post('logout',[\App\Http\Controllers\Api\v1\Auth\AuthController::class,'logout'])->middleware('auth:api');
        Route::get('test',[\App\Http\Controllers\Api\v1\Auth\AuthController::class,'test']);
        Route::get('home',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'index']);

        Route::prefix('operate')->middleware('auth:api')->group(function (){
            Route::apiResource('category',\App\Http\Controllers\Api\v1\CategoryController::class);
            Route::apiResource('book',\App\Http\Controllers\Api\v1\BookController::class);
            Route::apiResource('end_date',\App\Http\Controllers\Api\v1\EndateController::class);
            Route::get('/cekProfileAdmin',[\App\Http\Controllers\Api\v1\ProfileController::class,'cekAdmin']);

            Route::post('book/edit/{id}',[\App\Http\Controllers\Api\v1\BookController::class,'update']);
            Route::get('book/{book}',[\App\Http\Controllers\Api\v1\BookController::class,'show']);
            Route::get('test',[\App\Http\Controllers\Api\v1\Auth\AuthController::class,'test']);
            Route::get('/histories',[\App\Http\Controllers\Api\v1\historyAdminController::class,'index']);
            Route::post('/returnAdmin',[\App\Http\Controllers\Api\v1\ProfileController::class,'returnAdmin']);
            Route::post('/actionReturn',[\App\Http\Controllers\Api\v1\ProfileController::class,'actionRetrun']);
            Route::post('/actionPengambilan',[\App\Http\Controllers\Api\v1\ProfileController::class,'actionPengambilan']);
            Route::post('/getPengambilan',[\App\Http\Controllers\Api\v1\ProfileController::class,'getPengambilan']);

        });

    });

    Route::prefix('client')->middleware('cors')->group(function (){
        Route::post('regis',[\App\Http\Controllers\Api\v1\Auth\ClientController::class,'regis']);
        Route::post('login',[\App\Http\Controllers\Api\v1\Auth\ClientController::class,'login']);
        Route::post('logout',[\App\Http\Controllers\Api\v1\Auth\ClientController::class,'logout'])->middleware('auth:customer-api');
        Route::get('test',[\App\Http\Controllers\Api\v1\Auth\ClientController::class,'test']);

        Route::get('home',[\App\Http\Controllers\Api\v1\landing\HomeController::class,'index']);

        Route::prefix('operate')->middleware(['auth:customer-api'])->group(function (){

            Route::post('return',[\App\Http\Controllers\Api\v1\ProfileController::class,'return']);
            Route::get('cekProfile',[\App\Http\Controllers\Api\v1\ProfileController::class,'cek']);
            Route::get('histories',[\App\Http\Controllers\Api\v1\ProfileController::class,'histories']);

            Route::post('cart',[\App\Http\Controllers\Api\v1\CartController::class,'actionCart']);
            Route::get('cart',[\App\Http\Controllers\Api\v1\CartController::class,'getCart']);
            Route::delete('cart/destroy/{id}',[\App\Http\Controllers\Api\v1\CartController::class,'deleteCart']);
            Route::post('rent',[\App\Http\Controllers\Api\v1\CartController::class,'rent']);

        });
    });

});

